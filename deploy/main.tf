terraform {
  backend "s3" {
    bucket         = "ambika-terraformstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraformstate"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
	Environment = terraform.workspace
	Project = var.Project 
	Owner = var.Contact
	Managedby = "Terraform"
}
}








