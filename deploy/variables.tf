variable "prefix" {
  default = "apiapp"
}

variable "Project" {
    default = "recipe-app-api-devops"
}

variable "Contact" {
    default = "xyz@fmt.com"

}
